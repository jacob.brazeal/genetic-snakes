export const WINDOW_RADIUS = 2;
export const cellTypes = {
    'EMPTY': 0,
    'DEATH': 1,
    'FOOD': 2,
    'SNAKE': 3
}

export const DIRECTIONS = ['LEFT', 'FORWARD', 'RIGHT'];

export function generateRandomDNA() {
    let DNA = {};
    for (let i = -WINDOW_RADIUS; i < WINDOW_RADIUS + 1; ++i) {
        DNA[i] = {};
        for (let j = 0; j < WINDOW_RADIUS + 1; ++j) {
            DNA[i][j] = Object.fromEntries(Object.keys(cellTypes).map(type => [
                type, Object.fromEntries(DIRECTIONS.map(d => [d, Math.random()]))
            ]))
        }
    }
    return DNA;
}

