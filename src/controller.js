import React, {Component} from 'react';
import {generateRandomDNA} from "./helper";
import SnakeGame from "./game";

export default class Controller extends Component {
    constructor(props) {
        super(props);

        const generations = 100, perGeneration = 25;
        const initialDNA = new Array(perGeneration).fill(0).map(() => generateRandomDNA());

        this.resultsByGeneration = [[]];

        this.state = {
            generations: generations,
            perGeneration: perGeneration,
            dna: initialDNA,
            round: 0
        }

    }

    nextGeneration() {
        const winners = this.resultsByGeneration[this.state.round].sort((a, b) => {
            if (a.stats.foods === b.stats.foods) {
                return a.stats.iters > b.stats.iters ? -1 : 1
            }
            return a.stats.foods > b.stats.foods ? -1 : 1
        })

        const passThroughCnt = 3;

        const randomMutatedDNA=(mutations = 10)=> {
            let sourceDNA = JSON.parse(JSON.stringify(this.state.dna[winners.slice(0, passThroughCnt)[Math.random() * passThroughCnt | 0].id]));
            while (mutations-- > 0) {
                // 1. Pick a random spot
                let root = sourceDNA;
                while (root && typeof Object.values(root)[0] === 'object') {
                    let kids = Object.values(root);
                    root = kids[Math.random() * kids.length | 0];
                }
                let keys = Object.keys(root);
                let key = keys[Math.random() * keys.length | 0];
                root[key] = (root[key] + Math.random()) % 1;
            }
            return sourceDNA;
        }

        let nextDNAs = [];
        nextDNAs = nextDNAs.concat(winners.slice(0, passThroughCnt).map(({id})=>this.state.dna[id]));
        nextDNAs = nextDNAs.concat(new Array(winners.length - passThroughCnt).fill(0).map(() => randomMutatedDNA()))
        console.log("Next DNAs: ", nextDNAs);
        // 2. Next generation
        this.resultsByGeneration.push([])
        this.setState({
            round: this.state.round + 1,
            dna: nextDNAs
        })
    }

    render() {
        return <div className={"controller"}>
            <div className={"controller-head"}>
                <h2>Generation {this.state.round + 1} / {this.state.generations}
                    { this.state.round > 0 ? <span> | Best survival: {this.resultsByGeneration[this.state.round - 1][0].stats.iters} with length {this.resultsByGeneration[this.state.round - 1][0].stats.foods}</span>: ''}
                    </h2>
            </div>
            <div key={this.state.round} className={"controller-games"}>{
                this.state.dna.map((x, i) => <SnakeGame delay={300} round={this.state.round} onFinish={x => this.onFinish(i, x)} key={i} dna={x}/>)
            }</div>
        </div>
    }

    onFinish(id, stats) {
        this.resultsByGeneration[this.state.round][id] = {
            id: id,
            stats: stats
        };

        if (Object.keys(this.resultsByGeneration[this.state.round]).length >= this.state.perGeneration) {
            // 1. Tabulate results
            if (this.state.round < this.state.generations - 1) {
                this.nextGeneration()
            }
        }
    }
}