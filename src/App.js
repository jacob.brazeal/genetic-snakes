import logo from './logo.svg';
import './App.css';
import SnakeGame from "./game";
import Controller from './controller'

function App() {
    return (
        <div className={"snake-game"}>
            <h1>Snake Game: Genetic Algorithms Demo</h1>
            <Controller/>
        </div>
    );
}

export default App;
