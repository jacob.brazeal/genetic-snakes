import React, {Component} from 'react'
import {WINDOW_RADIUS, cellTypes, DIRECTIONS} from "./helper";

const map = require('./map.json');


const idToType = Object.fromEntries(Object.entries(cellTypes).map(([key, num]) => [num, key]));

export default class SnakeGame extends Component {
    constructor(props) {
        super(props);

        this.secretId = Math.random().toString(16).slice(2);
        this.history = [];
        const headCoords = this.extractHeadCoords(map.grid);
        this.state = {
            isDead: false,
            map: map.grid,
            headCoords: headCoords,
            snakeCoords: [headCoords],
            headDir: map.direction,
            dna: this.props.dna,
            window: this.getWindow(map.grid, map.direction, headCoords),
            foods: 0,
            iter: 0
        }


    }

    componentDidMount() {
        this.play();
    }

    play() {
        if (this.state.isDead) {
            if (this.props.onFinish) {
                this.props.onFinish({
                    foods: this.state.foods,
                    iters: this.state.iter
                })
            }
            return;
        }
        if (this.history.find(x => x.headCoords.toString() === this.state.headCoords.toString() && x.foods === this.state.foods)) {
            this.setState({
                isDead: true
            })
        }
        this.history.push({
            headCoords: this.state.headCoords.slice(),
            foods: this.state.foods
        })

        window.pcnt = window.pcnt || 0;
        window.secretIds = window.secretIds || new Set();
        window.secretIds.add(this.secretId);
        window.pcnt += 1;
        console.log(window.pcnt, window.secretIds.size);

        // 1. Get environment information
        let env = this.state.window;
        let dna = this.state.dna;

        // 2. Score each direction
        let bestDir, bestScore = -Infinity;
        for (const direction of DIRECTIONS) {
            let score = 0;
            for (let i = -WINDOW_RADIUS; i < WINDOW_RADIUS + 1; ++i) {
                for (let j = 0; j < WINDOW_RADIUS + 1; ++j) {
                    let val = dna[i][j][env[i][j]][direction];
                    score += val;
                }
            }
            if (score > bestScore) {
                bestDir = direction;
                bestScore = score;
            }
        }

        console.log(bestDir, bestScore);
        // 3. Execute direction with best score

        // 30. Get actual coordinates of that direction
        let newDir = this.directionToDelta(bestDir);
        let nextHead = this.deltaToCoords([0, 1], newDir);
        // 3a.
        const didEat = this.getPos(nextHead[0], nextHead[1]) === cellTypes.FOOD;
        const didDie = [cellTypes.DEATH, cellTypes.SNAKE].includes(this.getPos(nextHead[0], nextHead[1]));
        let nextSnakeCoords = this.state.snakeCoords.concat([nextHead]);
        let nextGrid = JSON.parse(JSON.stringify(this.state.map));
        nextGrid[nextHead[0]][nextHead[1]] = cellTypes.SNAKE;
        if (!didEat) {
            if (!(nextGrid && nextGrid[nextSnakeCoords[0][0]])) debugger;
            nextGrid[nextSnakeCoords[0][0]][nextSnakeCoords[0][1]] = cellTypes.EMPTY;
            nextSnakeCoords = nextSnakeCoords.slice(1);
        }

        let nextFoods = didEat ? this.state.foods + 1 : this.state.foods;
        let nextIter = this.state.iter + 1;

        this.setState({
            isDead: this.state.isDead || didDie,
            map: nextGrid,
            snakeCoords: nextSnakeCoords,
            foods: nextFoods,
            headCoords: nextHead,
            headDir: newDir,
            window: this.getWindow(nextGrid, newDir, nextHead),
            iter: nextIter
        })

        // 4. At some point in future, continue
        setTimeout(() => this.play(), this.props.delay);

    }

    componentWillUnmount() {
        this.setState({
            isDead: true
        })
    }

    getPos(i, j, grid = this.state.map) {
        if (i < 0 || j < 0 || i >= grid.length || j >= grid.length) {
            return cellTypes.DEATH;
        }
        return grid[i][j];
    }

    directionToDelta(direction) {
        let rot = {
            'LEFT': [-1, 1],
            'RIGHT': [-1, -1],
            'FORWARD': [0, 0]
        }[direction];

        let [dx, dy] = this.state.headDir;

        // TODO: convert to nice matrix multiplication here
        if (dx === -1 && dy === 0) { // Up
            switch (direction) {
                case 'LEFT':
                    return [0, -1];
                case 'RIGHT':
                    return [0, 1];
                case 'FORWARD':
                default:
                    return [-1, 0]
            }

        } else if (dx === 1 && dy === 0) {
            switch (direction) {
                case 'LEFT':
                    return [0, 1];
                case 'RIGHT':
                    return [0, -1];
                case 'FORWARD':
                default:
                    return [1, 0]
            }
        } else if (dx === 0 && dy === -1) {
            switch (direction) {
                case 'LEFT':
                    return [1, 0];
                case 'RIGHT':
                    return [-1, 0];
                case 'FORWARD':
                default:
                    return [0, -1]
            }
        } else if (dx === 0 && dy === 1) {
            switch (direction) {
                case 'LEFT':
                    return [-1, 0];
                case 'RIGHT':
                    return [1, 0];
                case 'FORWARD':
                default:
                    return [0, 1]
            }
        }
        throw "Invalid direction";
    }

    deltaToCoords([i, j], dir = this.state.headDir) {
        let [x, y] = this.state.headCoords;
        let [dx, dy] = dir

        let gridRowPos, gridColPos;
        if (dx === -1 && dy === 0) { // Headed up
            gridRowPos = x - j;
            gridColPos = y + i;
        } else if (dx === 1 && dy === 0) { // Headed down
            gridRowPos = x + j;
            gridColPos = y - i;
        } else if (dx === 0 && dy === -1) { // Head left
            gridRowPos = x - i;
            gridColPos = y - j;
        } else { // Headed right
            gridRowPos = x + i;
            gridColPos = y + j;
        }
        return [gridRowPos, gridColPos]

    }

    getWindow(grid = this.state.map, headDir = this.state.headDir, headCoords = this.state.headCoords) {
        let window = {};
        let [dx, dy] = headDir;
        let [x, y] = headCoords;
        for (let i = -WINDOW_RADIUS; i < WINDOW_RADIUS + 1; ++i) { // Perpendicular to direction of snake movement

            window[i] = {};
            for (let j = 0; j < WINDOW_RADIUS + 1; ++j) { // Parallel to direction of snake movement
                let gridRowPos, gridColPos;
                if (dx === -1 && dy === 0) { // Headed up
                    gridRowPos = x - j;
                    gridColPos = y + i;
                } else if (dx === 1 && dy === 0) { // Headed down
                    gridRowPos = x + j;
                    gridColPos = y - i;
                } else if (dx === 0 && dy === -1) { // Head left
                    gridRowPos = x - i;
                    gridColPos = y - j;
                } else { // Headed right
                    gridRowPos = x + i;
                    gridColPos = y + j;
                }

                window[i][j] = idToType[this.getPos(gridRowPos, gridColPos, grid)];
            }
        }
        return window;
    }

    extractHeadCoords(grid) {
        for (let i = 0; i < grid.length; ++i) {
            for (let j = 0; j < grid[i].length; ++j) {
                if (grid[i][j] === cellTypes.SNAKE) {
                    return [i, j]
                }
            }
        }
        throw "Snake not found";
    }

    render() {
        return <div className={"game-wrap " + (this.state.isDead ? 'game-wrap--dead' : '')}>
            {
                this.state.map.map((row, i) => <div key={i} className={"game-row"}>
                    {
                        row.map((x, j) => <div key={j} className={"game-col"}><Cell cellType={x}/></div>)
                    }
                </div>)
            }

        </div>
    }
}

function Cell(props) {
    return <div className={"game-cell game-cell--" + idToType[props.cellType]}/>

}